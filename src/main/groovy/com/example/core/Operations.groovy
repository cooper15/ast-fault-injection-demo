package com.example.core

import com.example.transformations.FaultyInitialize

@FaultyInitialize(attribute = 'a')
class Operations {
    int a
    int b
}
