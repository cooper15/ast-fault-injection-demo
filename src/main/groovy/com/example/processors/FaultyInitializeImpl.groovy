package com.example.processors

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

import java.lang.reflect.Modifier

@GroovyASTTransformation(phase = CompilePhase.INSTRUCTION_SELECTION)
class FaultyInitializeImpl extends AbstractASTTransformation {
    @Override
    void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
        AnnotationNode annotation = (AnnotationNode) astNodes[0] // Annotation FaultyInitialize
        ClassNode classNode = (ClassNode) astNodes[1]  // Clase anotada

        String fieldName = ((ConstantExpression) annotation.getMember('attribute')).value

        Expression thisKeyword = new VariableExpression('this')
        Expression propertyName = new ConstantExpression(fieldName)

        // crea un metodo que intercepta el setter https://groovy-lang.org/metaprogramming.html#_getsetmetaclass
        Expression setPropertyExpression = new MethodCallExpression(
                new PropertyExpression(
                        thisKeyword,
                        'metaClass'
                ),
                'setProperty',
                new ArgumentListExpression([thisKeyword, propertyName, new ConstantExpression(9)])
        )

        Statement methodBody = new ExpressionStatement(
                setPropertyExpression
        )

        /* Basado en lo anterior el metodo es creado con la siguiente estructura
        *
        * public void setProperty(String name, Object value){
        *    this.metaClass.setProperty('valor del attribute linea 21', 9)
        * }
        * */
        MethodNode method = new MethodNode(
                'setProperty',
                Modifier.PUBLIC,
                ClassHelper.VOID_TYPE,
                [new Parameter(ClassHelper.STRING_TYPE, 'name'),
                 new Parameter(ClassHelper.OBJECT_TYPE, 'value')] as Parameter[],
                [] as ClassNode[],
                methodBody
        )
        classNode.addMethod(method)
    }
}
