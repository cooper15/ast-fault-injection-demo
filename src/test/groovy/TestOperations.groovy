import com.example.core.Operations
import org.testng.annotations.Test

import static org.hamcrest.CoreMatchers.equalTo
import static org.hamcrest.MatcherAssert.assertThat

class TestOperations {


    @Test
    void test001() {
        Operations op = new Operations()
        op.a = 8
        assertThat(op.a, equalTo(9))
    }
}
